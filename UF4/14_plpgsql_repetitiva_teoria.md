# PL/pgSQL: Estructura repetitiva

## LOOP

És el bucle més senzill. Repeteix les sentències infinitament fins que troba la sentència EXIT.

Sintaxi:
```
[ <<label>> ]
LOOP
    statements;
    EXIT [ label ] [ WHEN boolean-expression ];
END LOOP [ label ];
```

Exemple:
```
CREATE OR REPLACE FUNCTION count_letter(s text, l char) RETURNS int AS $$
   DECLARE
       cur_letter char;
       count int := 0;
       i int := 1;
   BEGIN
       LOOP
           cur_letter := substring(s, i, 1);       
           IF cur_letter = l THEN
               count := count + 1;
           END IF;
           i := i + 1;
           IF i > length(s) THEN
               EXIT;
           END IF;
       END LOOP;
       RETURN count;
   END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION count_letter(s text, l char) RETURNS int AS $$
   DECLARE
       cur_letter char;
       count int := 0;
       i int := 1;
   BEGIN
       LOOP
           cur_letter := substring(s, i, 1);       
           IF cur_letter = l THEN
               count := count + 1;
           END IF;
           i := i + 1;
           EXIT WHEN i > length(s);
       END LOOP;
       RETURN count;
   END;
$$ LANGUAGE 'plpgsql';
```

## WHILE

Sintaxi:
```
[ <<label>> ]
WHILE boolean-expression LOOP
    statements
END LOOP [ label ];
```

Exemple:
```
CREATE OR REPLACE FUNCTION pow(base numeric, exp int) RETURNS numeric AS $$
   DECLARE
       result numeric := 1;
       i int := 1;
   BEGIN
       WHILE i <= exp LOOP
           result := base * result;
           i := i + 1;
       END LOOP;
       RETURN result;
   END;
$$ LANGUAGE 'plpgsql';
```

## FOR

### Bucles amb enters

Sintaxi:
```
[ <<label>> ]
FOR name IN [ REVERSE ] expression .. expression [ BY expression ] LOOP
    statements
END LOOP [ label ];
```

Exemples:
```
CREATE OR REPLACE FUNCTION nums() RETURNS SETOF int AS $$
   DECLARE
       i int;
   BEGIN
       FOR i IN 1..10 LOOP
          RETURN NEXT i;
       END LOOP;
       RETURN;
   END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION nums_2() RETURNS SETOF int AS $$
   DECLARE
       i int;
   BEGIN
       FOR i IN 1..10 BY 2 LOOP
          RETURN NEXT i;
       END LOOP;
       RETURN;
   END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION nums_desc() RETURNS SETOF int AS $$
   DECLARE
       i int;
   BEGIN
       FOR i IN REVERSE 10..1 LOOP
          RETURN NEXT i;
       END LOOP;
       RETURN;
   END;
$$ LANGUAGE 'plpgsql';
```


### Recórrer resultats de consultes

Sintaxi:
```
[ <<label>> ]
FOR target IN query LOOP
    statements
END LOOP [ label ];
```

Exemples:
```
CREATE OR REPLACE FUNCTION empl_este() RETURNS SETOF repventas AS $$
   DECLARE
       empl RECORD;
   BEGIN
       FOR empl IN SELECT repventas.* 
                   FROM repventas 
                   JOIN oficinas
                   ON oficina_rep = oficina
                   WHERE region = 'Este'
                LOOP
          RETURN NEXT empl;
       END LOOP;
       RETURN;
   END;
$$ LANGUAGE 'plpgsql';
-- Aquesta funció es podria fer més simple usant directament RETURN QUERY

CREATE OR REPLACE FUNCTION baja_ventas() RETURNS void AS $$
   DECLARE
       p RECORD;
       v numeric;
   BEGIN
       -- Recorrem totes les comandes recuperant el venedor que les ha fet i 
       -- l'import de la comanda
       FOR p IN SELECT rep, importe 
                FROM pedidos
             LOOP              
          -- Recuperem les vendes actuals del venedor
          SELECT ventas
          INTO v
          FROM repventas
          WHERE num_empl = p.rep;
          -- Li restem l'import de la comanda
          v := v - p.importe;
          -- Si en restar-li ens dóna un valor negatiu, el deixem a 0
          IF v < 0 THEN
              v := 0;
          END IF;              
          -- Li restem al venedor l'import de la comanda de les seves vendes
          UPDATE repventas
          SET ventas = v
          WHERE num_empl = p.rep;          
       END LOOP;
       RETURN;
   END;
$$ LANGUAGE 'plpgsql';
```
