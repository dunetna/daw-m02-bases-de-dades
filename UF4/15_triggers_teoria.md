# Triggers

Un *trigger* és una acció que s'executarà automàticament quan fem una alteració a una taula o a una vista.

En les taules, els triggers s'executen abans o després d'un INSERT, UPDATE o DELETE. Es pot fer un únic cop per sentǹcia o per cada fila afectada. En els triggers d'UPDATE es pot concretar que només s'executin si s'afecten columnes concretes.

En les vistes, es poden definir triggers per tal de substituir les comandes INSERT, UPDATE o DELETE. També podem definir triggers per tal que s'excutin un cop abans d'un INSERT, UPDATE o DELETE.

## Creació de triggers

Sintaxi (simplificada):

```
CREATE TRIGGER name { BEFORE | AFTER | INSTEAD OF } event ON table_name
    [ FOR [ EACH ] { ROW | STATEMENT } ]
    [ WHEN ( condition ) ]
    EXECUTE PROCEDURE function_name ( arguments )

where event can be one of:

    INSERT
    UPDATE [ OF column_name [, ... ] ]
    DELETE
    TRUNCATE
```

Exemples:

```
-- Executa la funció check_account_update abans de modificar qualsevol fila de la taula accounts:
CREATE TRIGGER check_update
    BEFORE UPDATE ON accounts
    FOR EACH ROW
    EXECUTE PROCEDURE check_account_update();

-- El mateix però només si la columna balance es modifica:
CREATE TRIGGER check_update
    BEFORE UPDATE OF balance ON accounts
    FOR EACH ROW
    EXECUTE PROCEDURE check_account_update();

-- El mateix però es comprova que realment el valor de la columna balance es modifica:
CREATE TRIGGER check_update
    BEFORE UPDATE ON accounts
    FOR EACH ROW
    WHEN (OLD.balance IS DISTINCT FROM NEW.balance)
    EXECUTE PROCEDURE check_account_update();

-- Es crida una funció després de l'UPDATE però només si alguna cosa canvia:
CREATE TRIGGER log_update
    AFTER UPDATE ON accounts
    FOR EACH ROW
    WHEN (OLD.* IS DISTINCT FROM NEW.*)
    EXECUTE PROCEDURE log_account_update();

-- Executa la funció view_insert_row quan volem inserir files a la vista (de fet la funció view_insert_row farem que faci INSERT a les taules afectades per la vista):
CREATE TRIGGER view_insert
    INSTEAD OF INSERT ON my_view
    FOR EACH ROW
    EXECUTE PROCEDURE view_insert_row();
```

## Activar i desactivar triggers

Desactivar un trigger:
```
ALTER TABLE table_name DISABLE TRIGGER trigger_name;
```

Activar un trigger:
```
ALTER TABLE table_name ENABLE TRIGGER trigger_name;
```

## Funcions que retornen triggers

Són funcions similars a les que hem fet amb les següents característiques:
* S'ha de definir abans de crear el trigger. 
* No ha de tenir paràmetres d'entrada.
* Ha de retornar el tipus trigger.
* Les funcions han de retornar el següent:
  * Si el trigger s'executa un sol cop la funció ha de retornar NULL.
  * Si el trigger s'executa per cada fila, la funció retorna NULL per no fer l'operació per aquella fila. Si es vol fer l'operació, es retornarà la fila corresponent.
* Disposem de diverses variables predeterminades que ens ajudaran a codificar la funció. Les més importants són:
  * NEW: variable de tipus RECORD que conté la nova fila després d'executar una operació INSERT o UPDATE.
  * OLD: variable de tipus RECORD que conté la fila antiga abans d'executar una operació INSERT o UPDATE.
  * TG_*: variables que es refereixen al trigger que ha disparat la funció. Mirar documentació.
  
Exemple:
```
CREATE TABLE emp (
    empname text,
    salary integer,
    last_date timestamp,
    last_user text
);

CREATE FUNCTION emp_stamp() RETURNS trigger AS $$
    BEGIN
        -- Check that empname and salary are given
        IF NEW.empname IS NULL THEN
            RAISE EXCEPTION 'empname cannot be null';
        END IF;
        IF NEW.salary IS NULL THEN
            RAISE EXCEPTION '% cannot have null salary', NEW.empname;
        END IF;

        -- Who works for us when she must pay for it?
        IF NEW.salary < 0 THEN
            RAISE EXCEPTION '% cannot have a negative salary', NEW.empname;
        END IF;

        -- Remember who changed the payroll when
        NEW.last_date := current_timestamp;
        NEW.last_user := current_user;
        RETURN NEW;
    END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER emp_stamp BEFORE INSERT OR UPDATE ON emp
    FOR EACH ROW EXECUTE PROCEDURE emp_stamp();
```

Exemples:
```
-- Validació de les dades d'entrada d'una taula 
CREATE TABLE emp (
    empname text,
    salary integer,
    last_date timestamp,
    last_user text
);

CREATE FUNCTION emp_stamp() RETURNS trigger AS $$
    BEGIN
        -- Check that empname and salary are given
        IF NEW.empname IS NULL THEN
            RAISE EXCEPTION 'empname cannot be null';
        END IF;
        IF NEW.salary IS NULL THEN
            RAISE EXCEPTION '% cannot have null salary', NEW.empname;
        END IF;

        -- Who works for us when she must pay for it?
        IF NEW.salary < 0 THEN
            RAISE EXCEPTION '% cannot have a negative salary', NEW.empname;
        END IF;

        -- Remember who changed the payroll when
        NEW.last_date := current_timestamp;
        NEW.last_user := current_user;
        RETURN NEW;
    END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER emp_stamp_trigger BEFORE INSERT OR UPDATE ON emp
    FOR EACH ROW EXECUTE PROCEDURE emp_stamp();
    
-- Trigger d'UPDATE d'una vista
CREATE VIEW vendedor_del_cliente AS 
   SELECT num_clie, empresa, num_empl, nombre 
   FROM clientes 
   JOIN repventas ON rep_clie = num_empl 
   ORDER BY num_clie;
      
CREATE OR REPLACE FUNCTION update_vendedor_del_cliente() RETURNS trigger AS $$
    BEGIN
        -- Si el codi del venedor no exiteix retornarem un error
        IF NEW.num_empl NOT IN (SELECT num_empl from REPVENTAS) THEN
            RAISE EXCEPTION 'El venedor % no existeix', NEW.num_empl;
        END IF;
        -- Si es modifica el número de client, l'empresa o el venedor del client,
        -- modifiquem la taula clientes
        UPDATE clientes
        SET num_clie = NEW.num_clie,
            empresa = NEW.empresa,
            rep_clie = NEW.num_empl
        WHERE num_clie = OLD.num_clie;
        -- Si es modifica el nom del venedor, modifiquem la taula repventas
        -- COMPTE: si no es modifica a NEW tenim el nom antic i es posaria 
        -- aquest nom al venedor nou!
        IF OLD.nombre <> NEW.nombre THEN
            UPDATE repventas
            SET nombre = NEW.nombre
            WHERE num_empl = NEW.num_empl;
        END IF;
        RETURN NULL;
    END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER update_vendedor_del_cliente_trigger INSTEAD OF UPDATE ON vendedor_del_cliente
    FOR EACH ROW EXECUTE PROCEDURE update_vendedor_del_cliente();
```
