# PL/pgSQL: Estructura alternativa

### IF/THEN

Sintaxi:
```
IF boolean-expression THEN
    statements
END IF;

IF boolean-expression THEN
    statements
ELSE
    statements
END IF;

IF boolean-expression THEN
    statements
[ ELSIF boolean-expression THEN
    statements
[ ELSIF boolean-expression THEN
    statements
    ...]]
[ ELSE
    statements ]
END IF;
```

Exemples:

```
CREATE FUNCTION stock_producto (f char, p char) RETURNS integer AS $$
  DECLARE
    stock integer;
  BEGIN
    SELECT existencias
    INTO stock
    FROM productos
    WHERE id_fab = f AND id_producto = p;
    
    IF NOT FOUND OR stock = 0 THEN
      RETURN -1;
    END IF;
    
    RETURN stock; 
  END;
$$ LANGUAGE 'plpgsql';

CREATE FUNCTION valor_producto (f char, p char) RETURNS text AS $$
  DECLARE
    total_producto numeric; 
    valor text;
  BEGIN
    SELECT existencias * precio
    INTO total_producto
    FROM productos
    WHERE id_fab = f AND id_producto = p;
    
    IF total_producto > 50000 THEN
      valor := 'Alto';
    ELSIF total_producto > 10000 THEN
      valor := 'Medio';
    ELSE
      valor := 'Bajo';
    END IF;
    
    RETURN valor; 
  END;
$$ LANGUAGE 'plpgsql';
```

### CASE

Sintaxi:
```
CASE search-expression
    WHEN expression [, expression [ ... ]] THEN
      statements
  [ WHEN expression [, expression [ ... ]] THEN
      statements
    ... ]
  [ ELSE
      statements ]
END CASE;

CASE
    WHEN boolean-expression THEN
      statements
  [ WHEN boolean-expression THEN
      statements
    ... ]
  [ ELSE
      statements ]
END CASE;
```

Exemples:

```
CREATE FUNCTION prima(ne smallint) RETURNS numeric AS $$
  DECLARE
     years int := 0;
     pr numeric := 0;
  BEGIN
     SELECT date_part('year', age(contrato))
     INTO years
     FROM repventas
     WHERE num_empl = ne; 
     
     CASE years
       WHEN 20 THEN
         pr := 2500;
       WHEN 40 THEN
         pr := 5000;
       ELSE
         pr := 500;
     END CASE;
     
     RETURN pr;
  END;
$$ LANGUAGE 'plpgsql';

CREATE FUNCTION valor_producto (f char, p char) RETURNS text AS $$
  DECLARE
    total_producto numeric; 
    valor text;
  BEGIN
    SELECT existencias * precio
    INTO total_producto
    FROM productos
    WHERE id_fab = f AND id_producto = p;
    
    CASE
      WHEN total_producto > 50000 THEN
        valor := 'Alto';
      WHEN total_producto > 10000 THEN
        valor := 'Medio';
      ELSE
        valor := 'Bajo';
    END CASE;
    RETURN valor; 
  END;
$$ LANGUAGE 'plpgsql';
```
