-- --------------------------------------------
--    triggers
-- --------------------------------------------


-- 1.- Es vol fer un control dels canvis produïts a la taula repventas. Per tal de fer-ho es crearà la taula cambios_repventas amb les següents columnes:
--  * usuario varchar(15): usuari de postgres que ha fet el canvi a la taula
--  * fecha date: data en la qual s'ha fet la modificaió
--  * num_empl_anterior smallint: codi de treballador que hi havia abans del canvi
--  * num_empl_nuevo smallint: codi de treballador que hi ha després del canvi
--  * nombre_anterior varchar(15): nom del treballador que hi havia abans del canvi
--  * nombre_nuevo varchar(15): nom del treballador que hi ha després del canvi
--
-- Crea un trigger que insereixi una fila en aquesta taula cada cop que s'insereixi, es modifiqui o s'esborri un treballador. Si s'insereix un treballador els camps num_empl_anterior i nombre_anterior seran NULL. Si s'esborra un treballador els camps num_empl_nuevo i nombre_nuevo seran NULL.


-- 2.- Crea un trigger que faci que no es permeti que un treballador pugui ser director de més de 5 treballadors.


-- 3.- Crea un trigger que faci que no es permeti que un treballador pugui ser director de més d'una oficina.


-- 4.- Crea un trigger que faci que no es permeti que es pugi el preu d'un producte més d'un 20% del preu actual.


-- 5.- Crea una taula de nom oficinas_baja amb la següent estructura:
--  * oficina smallint
--  * ciudad   character varying(15)
--  * region   character varying(10)
--  * dir      smallint
--  * objetivo numeric(9,2)
--  * ventas   numeric(9,2)
--  * usuario  character varying(15)
--  * fecha    date
--
-- Crea un trigger que insereixi una fila a la taula oficinas_baja cada cop que s'esborri una oficina. Les dades que s'inseriran són les corresponents a l'oficina que es dóna de baixa excepte les columnes usuario i fecha on es desaran l'usuari que dóna de baixa l'oficina i la data actual.


-- 6.- Crea un trigger que impedeixi que la quota total dels venedors d'una oficina (suma de les quotes de tots els seus venedors) sigui inferior a 200000.


-- 7.- Crea un trigger que no permeti inserir treballadors a l'oficina de Denver.


-- 8.- L'objectiu de l'exercici és fer que no s'eliminin mai les comandes. Per fer això farem les següents passes:
--
-- a) Modifica la taula pedidos afegint una columna de nom baja de tipus boolean. Si la comanda s'ha de donat de baixa tindra TRUE, altrament FALSE. Fes que quan s'insereixi una comanda aquest camp estigui per defecte a FALSE.
-- b) Crea un trigger que faci que quan s'esborri una comanda no l'esborri, però posi el camp baja de la comanda a TRUE.
-- c) No permetis que es modifiqui un registre que s'ha esborrat, a no ser que es tregui la marca d'esborrat.


-- 9.- Fer un trigger que actualitzi el camp existencias de la taula productos cada cop que s'insereixi o actualitzi una comanda. Es suposa que quan es ven una quantitat d'un producte, les seves existències baixen.


-- 10.- Crea els triggers necessaris per tal de fer les actualitzacions que consideris adients a la vista clientes_vip (exercici 4 del llistat d'exercicis de vistes).
