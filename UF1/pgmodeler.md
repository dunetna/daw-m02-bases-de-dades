# pgmodeler: instal·lació i configuració

## Instal·lació a Fedora 20

Baixar el paquet i desar-lo a /tmp: [http://rpm.hubbitus.info/archive/Fedora20/pgmodeler/x86_64/pgmodeler-0.8.0-1.fc20.x86_64.rpm](http://rpm.hubbitus.info/archive/Fedora20/pgmodeler/x86_64/pgmodeler-0.8.0-1.fc20.x86_64.rpm)

Instal·lar dependènices

```
# yum install qt5-qtbase qt5-qtbase-gui
```
Install·lar paquet
```
# rpm -i /tmp/pgmodeler-0.8.0-1.fc20.x86_64.rpm
```

# Configuració

Editem /var/lib/pgsql/data/pg_hba.conf
```
# IPv4 local connections:
host    all             all             127.0.0.1/32            md5
```

Reiniciem postgresql:
```
# systemctl restart postgresql
```

Posem contrasenya al nostre usuari
```
$ su postgres
Password: 
$ psql template1
psql (9.3.6)
Type "help" for help.

template1=# ALTER USER el_vostre_usuari PASSWORD 'postgres';
ALTER ROLE
template1=# \q
$ exit
```

Executem pg modeler i anem a *Settings*. Crear una nova connexió amb les següents dades

![Configuració de la connexió](pgmodeler-images/connection-config.png "Configuració de la connexió")

