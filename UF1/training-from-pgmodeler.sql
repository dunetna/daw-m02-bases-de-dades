-- Database generated with pgModeler (PostgreSQL Database Modeler).
-- pgModeler  version: 0.8.0
-- PostgreSQL version: 9.2
-- Project Site: pgmodeler.com.br
-- Model Author: ---

-- Database creation must be done outside an multicommand file.
-- These commands were put in this file only for convenience.
-- -- object: training | type: DATABASE --
-- -- DROP DATABASE IF EXISTS training;
-- CREATE DATABASE training
-- 	ENCODING = 'UTF8'
-- 	LC_COLLATE = 'en_US.UTF8'
-- 	LC_CTYPE = 'en_US.UTF8'
-- 	TABLESPACE = pg_default
-- 	OWNER = mramirez
-- ;
-- -- ddl-end --
-- 

-- object: public.clientes | type: TABLE --
-- DROP TABLE IF EXISTS public.clientes CASCADE;
CREATE TABLE public.clientes(
	num_clie smallint NOT NULL,
	empresa character varying(20) NOT NULL,
	rep_clie smallint NOT NULL,
	limite_credito numeric(8,2),
	CONSTRAINT clientes_pkey PRIMARY KEY (num_clie)

);
-- ddl-end --
ALTER TABLE public.clientes OWNER TO mramirez;
-- ddl-end --

-- object: public.oficinas | type: TABLE --
-- DROP TABLE IF EXISTS public.oficinas CASCADE;
CREATE TABLE public.oficinas(
	oficina smallint NOT NULL,
	ciudad character varying(15) NOT NULL,
	region character varying(10) NOT NULL,
	dir smallint,
	objetivo numeric(9,2),
	ventas numeric(9,2) NOT NULL,
	CONSTRAINT oficinas_pkey PRIMARY KEY (oficina)

);
-- ddl-end --
ALTER TABLE public.oficinas OWNER TO mramirez;
-- ddl-end --

-- object: public.pedidos | type: TABLE --
-- DROP TABLE IF EXISTS public.pedidos CASCADE;
CREATE TABLE public.pedidos(
	num_pedido integer NOT NULL,
	fecha_pedido date NOT NULL,
	clie smallint NOT NULL,
	rep smallint,
	fab character(3) NOT NULL,
	producto character(5) NOT NULL,
	cant smallint NOT NULL,
	importe numeric(7,2) NOT NULL,
	CONSTRAINT pedidos_pkey PRIMARY KEY (num_pedido)

);
-- ddl-end --
ALTER TABLE public.pedidos OWNER TO mramirez;
-- ddl-end --

-- object: public.productos | type: TABLE --
-- DROP TABLE IF EXISTS public.productos CASCADE;
CREATE TABLE public.productos(
	id_fab character(3) NOT NULL,
	id_producto character(5) NOT NULL,
	descripcion character varying(20) NOT NULL,
	precio numeric(7,2) NOT NULL,
	existencias integer NOT NULL,
	CONSTRAINT productos_pkey PRIMARY KEY (id_fab,id_producto)

);
-- ddl-end --
ALTER TABLE public.productos OWNER TO mramirez;
-- ddl-end --

-- object: public.repventas | type: TABLE --
-- DROP TABLE IF EXISTS public.repventas CASCADE;
CREATE TABLE public.repventas(
	num_empl smallint NOT NULL,
	nombre character varying(15) NOT NULL,
	edad smallint,
	oficina_rep smallint,
	titulo character varying(10),
	contrato date NOT NULL,
	director smallint,
	cuota numeric(8,2),
	ventas numeric(8,2) NOT NULL,
	CONSTRAINT repventas_pkey PRIMARY KEY (num_empl)

);
-- ddl-end --
ALTER TABLE public.repventas OWNER TO mramirez;
-- ddl-end --

-- object: rep_clie | type: CONSTRAINT --
-- ALTER TABLE public.clientes DROP CONSTRAINT IF EXISTS rep_clie CASCADE;
ALTER TABLE public.clientes ADD CONSTRAINT rep_clie FOREIGN KEY (rep_clie)
REFERENCES public.repventas (num_empl) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: dir | type: CONSTRAINT --
-- ALTER TABLE public.oficinas DROP CONSTRAINT IF EXISTS dir CASCADE;
ALTER TABLE public.oficinas ADD CONSTRAINT dir FOREIGN KEY (dir)
REFERENCES public.repventas (num_empl) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: clie | type: CONSTRAINT --
-- ALTER TABLE public.pedidos DROP CONSTRAINT IF EXISTS clie CASCADE;
ALTER TABLE public.pedidos ADD CONSTRAINT clie FOREIGN KEY (clie)
REFERENCES public.clientes (num_clie) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: rep | type: CONSTRAINT --
-- ALTER TABLE public.pedidos DROP CONSTRAINT IF EXISTS rep CASCADE;
ALTER TABLE public.pedidos ADD CONSTRAINT rep FOREIGN KEY (rep)
REFERENCES public.repventas (num_empl) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: prod | type: CONSTRAINT --
-- ALTER TABLE public.pedidos DROP CONSTRAINT IF EXISTS prod CASCADE;
ALTER TABLE public.pedidos ADD CONSTRAINT prod FOREIGN KEY (fab,producto)
REFERENCES public.productos (id_fab,id_producto) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: oficina_rep | type: CONSTRAINT --
-- ALTER TABLE public.repventas DROP CONSTRAINT IF EXISTS oficina_rep CASCADE;
ALTER TABLE public.repventas ADD CONSTRAINT oficina_rep FOREIGN KEY (oficina_rep)
REFERENCES public.oficinas (oficina) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: director | type: CONSTRAINT --
-- ALTER TABLE public.repventas DROP CONSTRAINT IF EXISTS director CASCADE;
ALTER TABLE public.repventas ADD CONSTRAINT director FOREIGN KEY (director)
REFERENCES public.repventas (num_empl) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --


