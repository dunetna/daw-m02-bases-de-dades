# El model relacional

Les **relacions** poden ser concebudes com a representacions tabulars de les dades.

La **clau primària** d'una relació distingeix unívocament cada tuple d’una relació de la resta.

Una **clau forana** està constituïda per un atribut, o per un conjunt d’atributs, de l’esquema d’una relació, que serveix per relacionar els seus tuples amb els tuples d’una altra relació de la base de dades (o amb els tuples d’ella mateixa, en alguns casos).

La **integritat referencial** implica que, per a qualsevol tuple, la combinació de valors que adopta el conjunt dels atributs que formen la clau forana de la relació o bé ha de ser present en la clau primària a la qual fa referència, o bé ha d’estar constituïda exclusivament per valors nuls (si els atributs implicats admeten aquesta possibilitat, i així s’ha estipulat en definir-ne les propietats). 

## Traducció del model Entitat-Relació al model relacional

### Entitats

Les entitats es transformen en relacions:
* Els atributs de l’entitat originària seran els atributs de la relació resultant.
* La clau primària de l’entitat originària serà la clau primària de la relació resultant.

**Model E-R:**

![clau primària](https://ioc.xtec.cat/materials/FP/Materials/2252_DAM/DAM_2252_M02/web/html/WebContent/u2/media/ic10m2u2_06.png)

**Model Relacional:**

ALUMNE(DNI, Nom, Cognoms)

### Interrelacions

Les interrelacions binàries de connectivitat 1-1 o 1-N originen claus foranes en relacions ja existents.

**1:1, Model E-R:**

![interrelació 1:1](http://ioc.xtec.cat/materials/FP/Materials/2252_DAM/DAM_2252_M02/web/html/WebContent/u3/media/ic10m2u3_03.png)

**Model Relacional:**

PROFESSOR(*DNI*, Nom, Cognoms)

DEPARTAMENT(*Codi*, Descripció, DNI-Professor) ON {DNIProfessor} REFERENCIA PROFESSOR

**1:n, Model E-R:**

![realció 1:n](http://ioc.xtec.cat/materials/FP/Materials/2252_DAM/DAM_2252_M02/web/html/WebContent/u3/media/ic10m2u3_04.png)

**Model Relacional:**

PROFESSOR(*DNI*, Nom, Cognoms, Codi-Departament) ON {Codi-Departament} REFERENCIA DEPARTAMENT i Codi-Departament ADMET VALORS NULS

DEPARTAMENT(*Codi*, Descripció) 

* Les interrelacions binàries de connectivitat M-N i totes les n-àries d’ordre superior a 2 sempre es transformen en noves relacions.

**m:n, Model E-R:**

![interrelació m:n](http://ioc.xtec.cat/materials/FP/Materials/2252_DAM/DAM_2252_M02/web/html/WebContent/u3/media/ic10m2u3_05.png)

**Model Relacional:**

ALUMNE(*DNI*, Nomm, Cognoms)

ESPORT(*Codi*, Descripcio)

PRACTICA(*DNI-Alumne*, *Codi-Esport*, DiaSetmana) ON {DNI-Alumne} REFERNCIA ALUMNE i {Codi-Esport} REFERENCIA ESPORT

**m:n:p, Model E-R:**

![interrelació m:n:p](http://ioc.xtec.cat/materials/FP/Materials/2252_DAM/DAM_2252_M02/web/html/WebContent/u3/media/ic10m2u3_06.png)

**Model Relacional:**

ALUMNE(*DNI*, Nomm, Cognoms)

ESPORT(*Codi*, Descripcio)

CURS(*Codi*)

PRACTICA(*DNI-Alumne*, *Codi-Esport*, *Codi-Curs*, DiaSetmana) ON {DNI-Alumne} REFERENCIA ALUMNE, {Codi-Esport} REFERENCIA ESPORT i {Codi-Curs} REFERENCIA CURS

**recursiva 1:n, Model E-R:**

![interrelació recursiva 1:n](http://ioc.xtec.cat/materials/FP/Materials/2252_DAM/DAM_2252_M02/web/html/WebContent/u3/media/ic10m2u3_10.png)

**Model Relacional:**

ALUMNE(*DNI*, Nom, Cognoms, DNI-Delegat) ON {DNI-Delegat} referencia ALUMNE

**recursiva m:n, Model E-R:**

![interrelació recursiva m:n](http://ioc.xtec.cat/materials/FP/Materials/2252_DAM/DAM_2252_M02/web/html/WebContent/u3/media/ic10m2u3_11.png)

**Model Relacional:**

ASSIGNATURA(*Codi*, Descripcio)

PRERREQUISIT(*Codi-Assignatura*,*Codi-Prerrequisit) ON {Codi-Assignatura} REFERENCIA ASSIGNATURA i {Codi-Prerrequisit} REFERENCIA ASSIGNATURA

### Generalització i especialització

En aquests casos, tant l’enti­tat super­classe com les enti­tats de tipus sub­classe es trans­for­men en noves rela­ci­ons. 

**Model E-R:**
![generalització especialització](http://ioc.xtec.cat/materials/FP/Materials/2252_DAM/DAM_2252_M02/web/html/WebContent/u3/media/ic10m2u3_14.png)

**Model Relacional:**

PERSONA(*DNI*, Nom, Cognom, Telefon)

PROFESSOR(*DNI*, Sou) ON {DNI} REFERENCIA PERSONA

ALUMNE(*DNI*) ON {DNI} REFERENCIA PERSONA

INFORMATIC(*DNI*, EspecialitatMaquinaria, EspecialitatProgramari) ON {DNI} REFERENCIA PROFESSOR

ADMINISTRATIU(*DNI*, Titulacio, Especialitat) ON {DNI} REFERENCIA PROFESSOR
