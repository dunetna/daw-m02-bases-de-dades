# Model Entitat - Relació

Resum dels [apunts de l'IOC](https://ioc.xtec.cat/materials/FP/Materials/2252_DAM/DAM_2252_M02/web/html/WebContent/u2/a1/continguts.html)

## Entitats

Una **entitat** és alguna cosa que existeix en el món real, distingible de la resta de coses, i de la qual ens interessen algunes propietats. 

![entitat](https://ioc.xtec.cat/materials/FP/Materials/2252_DAM/DAM_2252_M02/web/html/WebContent/u2/media/ic10m2u2_01.png)

## Atributs

Anomenem **atributs** les característiques que ens interessen de les entitats. 

![atributs](https://ioc.xtec.cat/materials/FP/Materials/2252_DAM/DAM_2252_M02/web/html/WebContent/u2/media/ic10m2u2_02.png)

Perquè un valor d’un atribut sigui vàlid, ha de pertànyer al conjunt de valors acceptables per a l’atribut en qüestió. Aquest conjunt de valors vàlids s’anomena **domini**. 

**Exemple:** El domini de l’atribut Nom de l’entitat ALUMNE podria consistir en el conjunt de totes les cadenes de caràcters possibles d’una longitud determinada, tot excloent les xifres i els caràcters especials. Serien valors vàlids per a l’atribut Nom, definit d’aquesta manera, “Laia”, “Pol”, etc.

## Clau primària

L'atribut o el conjunt d’atributs que identifiquen unívocament les entitats instància s’anomenen **clau primària** de l’entitat. 

![clau primària](https://ioc.xtec.cat/materials/FP/Materials/2252_DAM/DAM_2252_M02/web/html/WebContent/u2/media/ic10m2u2_06.png)

## Interrelació

Una **interrelació** consisteix en una associació entre dues o més entitats. 

**Exemple:** Un alumne es matricula d'assignatures

![relacio](https://ioc.xtec.cat/materials/FP/Materials/2252_DAM/DAM_2252_M02/web/html/WebContent/u2/media/ic10m2u2_07.png)

### Atributs d'una interrelació

De vegades, ens pot interessar reflectir algunes característiques de determinades relacions. La manera de fer-ho és afegir els atributs necessaris, com faríem si treballéssim amb entitats. Aquests atributs són els **atributs de la interrelació**.

**Exemple:** S'ha de saber la nota obtinguda per un alumne d'una assignatura.

![atribut de interrelació](https://ioc.xtec.cat/materials/FP/Materials/2252_DAM/DAM_2252_M02/web/html/WebContent/u2/media/ic10m2u2_08.png)

### Grau d'una interrelació

El **grau d’una interrelació* és el nombre d’entitats que aquesta associa. 

**Exemple d'interrelació de grau 3:** Fins ara, la interrelació Matricula només permet emmagatzemar una matrícula de cada alumne en cada assignatura, i el seu atribut NotaFinal només permet reflectir una sola nota final de curs.

Però aquest esquema no permet modelitzar el fet que un alumne es pot haver de matricular més d’un cop d’una mateixa assignatura (i obtenir una nota final en cada nova matrícula) fins a obtenir una qualificació igual o superior a l’aprovat.

Una manera d’aconseguir representar aquesta característica del món real consistiria a afegir, en nostre disseny, una nova entitat que fes referència a l’element temporal. La podríem anomenar CURS, per exemple.

I, a continuació, només cal que la interrelació Matricula (tot conservant l’atribut NotaFinal) interrelacioni tres entitats: ALUMNE, ASSIGNATURA i CURS.

I el nou esquema ja permetrà registrar matrícules successives d’un mateix alumne en una mateixa assignatura, però al llarg de diferents cursos acadèmics, amb les respectives qualificacions obtingudes. 

![interrelació de grau 3](https://ioc.xtec.cat/materials/FP/Materials/2252_DAM/DAM_2252_M02/web/html/WebContent/u2/media/ic10m2u2_09.png)

### Cardinalitat d'una interrelació

La **cardinalitat** d’una interrelació indica el tipus de correspondència que hi ha entre les ocurrències de les entitats que ella mateixa permet associar. 

Les relacions binàries poden oferir tres tipus de connectivitat:

* Un a un (1:1)
* Un a uns quants (1:N)
* Uns quants a uns quants (M:N)

Un 1 al costat d’una entitat indica que, com a màxim, només una de les seves instàncies (la qual podrà variar en cada cas) tindrà la possibilitat d’estar associada amb cadascuna de les instàncies de l’altra entitat. 

En canvi, una N (o una M) al costat d’una entitat indica que serà una pluralitat de les seves instàncies (les quals també podran variar en cada cas) la que tindrà la possibilitat d’estar associada amb cadascuna de les instàncies de l’altra entitat. 

1:1

![interrelació 1:1](https://ioc.xtec.cat/materials/FP/Materials/2252_DAM/DAM_2252_M02/web/html/WebContent/u2/media/ic10m2u2_10.png)

1:n

![realció 1:n](https://ioc.xtec.cat/materials/FP/Materials/2252_DAM/DAM_2252_M02/web/html/WebContent/u2/media/ic10m2u2_11.png)

m:n

![interrelació m:n](https://ioc.xtec.cat/materials/FP/Materials/2252_DAM/DAM_2252_M02/web/html/WebContent/u2/media/ic10m2u2_12.png)

Les relacions ternàries poden oferir quatre tipus de connectivitat:

* 1:1:1
* 1:1:N
* 1:M:N
* M:N:P

m:n:p

![interrelació m:n:p](https://ioc.xtec.cat/materials/FP/Materials/2252_DAM/DAM_2252_M02/web/html/WebContent/u2/media/ic10m2u2_14.png)

De vegades, pot resultar útil establir **límits mínims i màxims a les cardinalitats** de les relacions. Per fer-ho, només cal afegir una etiqueta del tipus mín..màx, per tal d’expressar els límits respectius, al costat de la línia que uneix cada entitat amb la interrelació.

Els valors mín i màx podran tenir els valors següents:

* 0, per indicar la possibilitat que no existeixi cap associació entre instàncies.
* Qualsevol nombre enter, per indicar un límit mínim o màxim concret de possibilitats d’associació entre instàncies.
* Un asterisc (*) o n, per indicar la possibilitat d’un nombre il·limitat d’associacions entre instàncies.

![límits cardinalitats](https://ioc.xtec.cat/materials/FP/Materials/2252_DAM/DAM_2252_M02/web/html/WebContent/u2/media/ic10m2u2_15.png)

### Interrelacions recursives

Una **interrelació recursiva** associa les instàncies d’una entitat amb altres instàncies de la mateixa entitat. 

![interrelació recursiva](https://ioc.xtec.cat/materials/FP/Materials/2252_DAM/DAM_2252_M02/web/html/WebContent/u2/media/ic10m2u2_16.png)

## Especialització i generalització

L’**especialització** permet reflectir l’existència d’una entitat general, anomenada entitat superclasse, que es pot especialitzar en diferents entitats subclasse.

![especialització](http://ioc.xtec.cat/materials/FP/Materials/2252_DAM/DAM_2252_M02/web/html/WebContent/u2/media/ic10m2u2_23.png)

La **generalització**, en canvi, és el resultat d’observar com diferents entitats preexistents comparteixen certes característiques comunes (és a dir, identitat d’atributs o d’interrelacions en les quals participen). 

![generalització](http://ioc.xtec.cat/materials/FP/Materials/2252_DAM/DAM_2252_M02/web/html/WebContent/u2/media/ic10m2u2_24.png)
