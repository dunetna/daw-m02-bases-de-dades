# Funcions d'usuari

## CASE - WHEN - ELSE - END

Especifica una estructura condicional dins d'una sentència SQL.

**Exemples**

Llistar els noms dels venedors i una columna que dessigni si són majors de 40 anys o no amb el text "Adulto" o "Joven" respectivament

```SQL
SELECT nombre, 
	CASE 
		WHEN edad>40 THEN 'Adulto' 
		ELSE 'Joven'
    END
FROM repventas;
```

Llistar els codis dels productes denotant si el preu és menor a 100 ('muy barato'), entre 100 i 1000 ('barato'), entre 1000 i 3000 ('caro') o més gran a 3000 ('muy caro').

```SQL
SELECT id_fab, id_producto, precio,
	CASE 
		WHEN precio < 100 THEN 'muy barato' 
		WHEN precio < 1000 THEN 'barato' 
        WHEN precio < 3000 THEN 'caro' 
        ELSE 'Muy caro' 
    END 
FROM productos;
```

Llistar els productes i especificar si són molt venuts (més de 2 comandes) o no. Ho especificarem posant el text 'muy vendido' o 'poco vendido'.

```SQL
SELECT id_fab, id_producto, 
	CASE 
		WHEN count(*) > 2 THEN 'muy vendido' 
		ELSE 'poco vendido' 
    END 
FROM productos 
JOIN pedidos ON id_fab = fab AND id_producto = producto 
GROUP BY id_fab, id_producto;
```

Llisteu nom, edat i vendes dels venedors que compleixin el següent si tenen menys de 40 anys només llistarem els venedors que tinguin vendes superiors a 200000, si tenen 40 anys o més només llistarem els venedors que tinguin unes vendes superiors a 300000.

```SQL
SELECT nombre, edad, ventas 
FROM repventas 
WHERE 
CASE 
    WHEN edad < 40 THEN ventas > 200000 
    ELSE ventas > 300000 
END;
```

Establiu la quota a 0 als menors de 35 anys i pugeu un 5% a la resta

```SQL
UPDATE repventas 
SET cuota = CASE 
                WHEN edad < 35 THEN 0 
                ELSE cuota * 1.05
            END;
```

## Funcions SQL

Es pot definir una funció que contingui un conjunt de sentències SQL.

Poden retornar:
* Sense retorn
* 1 valor
  * 1 valor simple
  * 1 valor compost:
    * paràmetres OUT(tipus o nom de taula o vista)
    * un tipus definit per l'usuari
    * el nom d'una taula o vista existent
* Conjunt de valors: 
  * SETOF *tipus*, on *tipus* és:
    * un tipus predefinit
    * un tipus definit per l'usuari
    * el nom d'una taula o vista existent
  * Una taula nova: RETURNS TABLE

### Sense retorn

Es retorna void.

**Exemples**

```SQL
CREATE OR REPLACE FUNCTION sube_cuota (integer, numeric) RETURNS void AS $$
    UPDATE repventas
    SET cuota = cuota + $2
    WHERE num_empl = $1;
$$ LANGUAGE SQL;

CREATE FUNCTION borra_jovenes() RETURNS void AS $$
    DELETE FROM repventas
    WHERE edad < 40;
$$ LANGUAGE SQL;
```

### Retorn d'un valor simple

Retorna la primera fila de l'ultima consulta feta:

**Exemples:**

Retorna el número 1:

```SQL
CREATE FUNCTION one() RETURNS integer AS $$
    SELECT 1;
$$ LANGUAGE SQL;

SELECT one();
```

Funció que retorni el nom del primer venedor ordenats alfabèticament per nom:

```SQL
CREATE FUNCTION nombre_vendedor1() RETURNS text AS $$
    SELECT nombre 
	FROM repventas
	ORDER BY nombre;
$$ LANGUAGE SQL;

SELECT nombre_vendedor1();
```

Suma de dos nombres passats per paràmetre:

```SQL
CREATE FUNCTION add_em(integer, integer) RETURNS integer AS $$
    SELECT $1 + $2;
$$ LANGUAGE SQL;
```

O a partir de la versió 9.2 de PostgreSQL:

```SQL
CREATE FUNCTION add_em(x integer, y integer) RETURNS integer AS $$
    SELECT x + y;
$$ LANGUAGE SQL;

SELECT add_em(1,2);
```

Funció que retorni la suma de les vendes fetes pels venedors majors a una edat determinada:

```SQL
CREATE FUNCTION sum_ventas_edad(integer) RETURNS numeric  AS $$
    SELECT SUM(ventas) 
    FROM repventas 
    WHERE edad > $1;
$$ LANGUAGE SQL;

SELECT sum_ventas_edad(40);
SELECT sum_ventas_edad(60);
```

Enlloc de posar el tipus concret podem referenciar el tipus d'una columna d'una taula:

Funció que donat un venedor ens retorni les seves vendes

```SQL
CREATE FUNCTION ventas_rep(smallint) RETURNS numeric(8,2) AS $$
    SELECT ventas 
    FROM repventas 
    WHERE num_empl=$1; 
$$ LANGUAGE SQL;
```
    
Es podrien referenciar els tipus, així si canvien en la taula, la funció segueix sent correcta:

```SQL
CREATE FUNCTION ventas_rep(repventas.num_empl%TYPE) RETURNS repventas.ventas%TYPE AS $$
    SELECT ventas 
    FROM repventas 
    WHERE num_empl=$1; 
$$ LANGUAGE SQL;
    
SELECT ventas_rep(101::smallint);
```

També podem cridar la funció passant-li el nom d'una columna dins un SELECT i la funció s'executarà per a cadascun dels valors de la columna. Per exemple, si volem veure les vendes dels directors d'oficina, mostrant la ciutat i les vendes:

```SQL
SELECT ciudad, ventas_rep(dir)
FROM oficinas;
```

Si la consulta no és un SELECT tenim dues opcions. Afegir un SELECT auxiliar o posar la clàusula RETURNING:

```SQL
CREATE FUNCTION sube_cuota (integer, numeric) RETURNS numeric AS $$
    UPDATE repventas
    SET cuota = cuota + $2
    WHERE num_empl = $1;
      
    SELECT 1.0;
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION sube_cuota (integer, numeric) RETURNS numeric AS $$
    UPDATE repventas
    SET cuota = cuota + $2
    WHERE num_empl = $1;
        
    SELECT cuota
    FROM repventas
    WHERE num_empl = $1;
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION sube_cuota (integer, numeric) RETURNS numeric AS $$
    UPDATE repventas
    SET cuota = cuota + $2
    WHERE num_empl = $1
    RETURNING cuota; 
$$ LANGUAGE SQL;
```

Una altra opció és fer que no retorni res.

### Retorn d'un valor compost amb paràmetres OUT

Els paràmetres OUT són paràmetres de sortida, es crea una fila amb els valors OUT:

```SQL
CREATE FUNCTION operations (IN x int, IN y int, OUT sum int, OUT prod int) AS $$
    SELECT x + y, x* y;
$$ LANGUAGE SQL;
```
Hi ha les diverses maneres d'obtenir els resultats:

```SQL
SELECT operations(3,7);
 operations 
------------
 (10,21)
(1 row)

SELECT (operations(3,7)).sum;
 sum 
-----
  10
(1 row)

SELECT (operations(3,7)).prod;
 prod 
------
   21
(1 row)

SELECT (operations(3,7)).*;
 sum | prod 
-----+------
  10 |   21
(1 row)

SELECT * FROM operations(3,7);
 sum | prod 
-----+------
  10 |   21
(1 row)
```

Donada una oficina, obtenir l'objectiu i les vendes:

```SQL
CREATE FUNCTION objetivo_ventas(oficina smallint, OUT objetivo numeric, OUT ventas numeric) AS $$
    SELECT objetivo, ventas
    FROM oficinas
    WHERE oficina = $1;
$$ LANGUAGE SQL;
```

### Retorn d'un valor compost d'un tipus definit per l'usuari

Si tenim un tipus compost definit podem usar aquest en la clausula RETURNS. Ens retornarà el valor compost d'aquest tipus.

Donada una oficina, obtenir l'objectiu i les vendes:

```SQL
CREATE TYPE objetivo_ventas_type AS (objetivo numeric, ventas numeric);

CREATE FUNCTION objetivo_ventas(oficina smallint) RETURNS objetivo_ventas_type AS $$
    SELECT objetivo, ventas
    FROM oficinas
    WHERE oficina = $1;
$$ LANGUAGE SQL;
```

### Retorn d'un valor compost a partir del nom d'una taula o vista existent

Si tenim una taula/vista, podem posar el nom d'aquesta a RETURNS. Se'ns retornarà un valor compost amb la mateixa estructura de la taula/vista.

Donada una oficina, obtenir l'objectiu i les vendes:

```SQL
CREATE TABLE objetivo_ventas_table (objetivo numeric, ventas numeric);

CREATE FUNCTION objetivo_ventas(oficina smallint) RETURNS objetivo_ventas_table AS $$
    SELECT objetivo, ventas
    FROM oficinas
    WHERE oficina = $1;
$$ LANGUAGE SQL;
```

Donada una oficina, obtenir l'objectiu i les vendes:

```SQL
CREATE VIEW objetivo_ventas_view AS 
    SELECT objetivo, ventas 
    FROM oficinas;

CREATE FUNCTION objetivo_ventas(oficina smallint) RETURNS objetivo_ventas_view AS $$
    SELECT objetivo, ventas
    FROM oficinas
    WHERE oficina = $1;
$$ LANGUAGE SQL;
```

Obtenir el venedor més jove de l'empresa. Si hi ha empat obtenir el primer ordenat alfabèticament.

```SQL
CREATE FUNCTION primer_vendedor_joven() RETURNS repventas AS $$
    SELECT *
    FROM repventas
    WHERE edad < 40
    ORDER BY nombre
    LIMIT 1;
$$ LANGUAGE SQL;
```

### Retorn d'un conjunt de files

Per retornar un conjunt de files s'usa la paraula clau *SETOF*.

### Retorn d'un conjunt de valors d'un tipus predefinit

Codis d'oficina de la regió indicada:

```SQL
CREATE FUNCTION oficinas_region(region varchar(10)) RETURNS SETOF smallint AS $$
    SELECT oficina
    FROM oficinas
    WHERE region = $1;
$$ LANGUAGE SQL
```

### Retorn d'un conjunt de valors d'un tipus definit per l'usuari

Objectiu i vendes de les oficines d'una regió determinada:

```SQL
CREATE TYPE objetivo_ventas_type AS (objetivo numeric, ventas numeric);

CREATE FUNCTION objetivo_ventas(region varchar) RETURNS SETOF objetivo_ventas_type AS $$
    SELECT objetivo, ventas
    FROM oficinas
    WHERE region = $1;
$$ LANGUAGE SQL;
```


### Retorn d'un conjunt de valors a partir del nom d'una taula o vista existent

Objectiu i vendes de les ofcines d'una regió determinada:

```SQL
CREATE TABLE objetivo_ventas_table (objetivo numeric, ventas numeric);

CREATE FUNCTION objetivo_ventas(region varchar) RETURNS SETOF objetivo_ventas_table AS $$
    SELECT objetivo, ventas
    FROM oficinas
    WHERE region = $1;
$$ LANGUAGE SQL;
```

Objectiu i vendes de les ofcines d'una regió determinada:

```SQL
CREATE VIEW objetivo_ventas_view AS 
    SELECT objetivo, ventas 
    FROM oficinas;

CREATE FUNCTION objetivo_ventas(region varchar(10)) RETURNS SETOF objetivo_ventas_view AS $$
    SELECT objetivo, ventas
    FROM oficinas
    WHERE region = $1;
$$ LANGUAGE SQL;
```
Totes les dades de les oficines d'una regió determinada:

```SQL
CREATE FUNCTION oficinas_region(region varchar) RETURNS SETOF oficinas AS $$
    SELECT *
    FROM oficinas
    WHERE region = $1;
$$ LANGUAGE SQL;
```
Totes les dades dels venedors joves.

```SQL
CREATE FUNCTION vendedores_jovenes() RETURNS SETOF repventas AS $$
    SELECT *
    FROM repventas
    WHERE edad < 40;
$$ LANGUAGE SQL;
```

### Retorn d'una taula nova

Per tornar una o més files, es pot retornar una taula definida "al vol".

Objectiu i vendes de les ofcines d'una regió determinada:

```SQL
CREATE FUNCTION objetivo_ventas(region varchar(10)) RETURNS TABLE (objetivo numeric, ventas numeric) AS $$
    SELECT objetivo, ventas
    FROM oficinas
    WHERE region = $1;
$$ LANGUAGE SQL;
```

