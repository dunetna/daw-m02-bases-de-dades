# Accés

Per accedir al SGBD PostgreSql ens hem d'autenticar. PostgreSql permet diversos tipus d'autenticació definits al fitxer */var/lib/pgsql/data/pg_hba.conf*.

A cada línia d'aquest fitxer s'especifica: 

* si s'accedeix localment o remotament
* a quines bases de dades es pot accedir
* quins usuaris hi poden accedir
* quines adreces (IP) poden accedir 
* el mètode d'autenticació

**Sintaxi:**
```
TYPE  DATABASE        USER            ADDRESS                 METHOD
```

o

```
TYPE  DATABASE        USER            IP-ADDRESS      IP-MASK             METHOD
```

**Exemples:**
```
# Permet connexió local a qualsevol base de dades als usuaris del sistema
local   all             all                                     peer

# Permet connexió local a qualsevol base de dades a qualsevol usuari sense posar contrasenya
local   all             all                                     trust

# Accés local (però amb connexió TCP/IP) per a usuaris del sistema
host    all             all             127.0.0.1/32            ident

# El mateix que l'anterior però usant màscara de xarxa
host    all             all             127.0.0.1       255.255.255.255     ident

# Permet connexió de qualsevol usuari de Postgres amb contrasenya des de 
# la màquina amb IP 192.168.12.10 connectar-se a la base de dades training 
host    training        all             192.168.12.10/32        md5

# Rebutja tota connexió des de la màquina amb IP 192.168.54.1
host    all             all             192.168.54.1/32         reject
```

Per dir qualsevol adreça de xarxa: 0.0.0.0/0

Per fer accessos remots, hem de dir a Postgresql que escolti a les adreces de xarxa que volem en el fitxer */var/lib/pgsql/data/postgresql.conf*:
```
# Per defecte
listen_addresses='localhost'

# Per escoltar tot:
listen_addresses='*'

# Per escoltar IPs determinades:
listen_addresses='192.168.0.24, 192.168.0.26'
``` 

Cada cop que toquem fitxers de configuració hem de reiniciar el servei de Postgresql:
```
# systemctl restart postgresql
```

Atenció, pot ser que no puguem accedir remotament per tenir més capes de seguretat. S'ha de comprovar:

* selinux: permissive o disabled
* systemctl disable|stop iptables
* systemctl disable|stop ip6tables
* systemctl disable|stop firewalld

## CREATE ROLE / CREATE USER

*Roles:* un rol és una entitat que posseeix objectes de la base de dades i que té certs privilegis. Depen de com s'usa un rol es pot interpretar com a un usuari o com a un grup. 

\du: llista rols existents

Per crear rols:
```
CREATE ROLE nom_rol opcions;
```

Per crear un rol que pugui autenitcar-se li hem de posar l'opció LOGIN o usar CREATE USER:

```
CREATE ROLE nom_usuari LOGIN;

CREATE USER nom_usuari;
```

Algunes opcions:

* SUPERUSER|NOSUPERUSER: té permisos per a tot o no
* CREATEDB|NOCREATEDB: pot crear base de dades o no
* CREATEROLE|NOCREATEROLE: pot crear altres rols o no
* INHERIT|NOINHERIT: hereta els privilegis dels rols d'on n'és membre o no
* LOGIN|NOLOGIN: pot fer login o no
* PASSWORD: establim una contrasenya per al rol
* CONNECTION LIMIT num: nombre màxim de connexions simultànies
* VALID UNTIL 'timestamp': després d'aquesta data la contrasenya ja no serà vàlida
* IN ROLE role_name: el rol que creem s'afegix al rol existent (i tindrà els seus privilegis, sempre i quan hi hagi l'opció INHERIT, que està per defecte).


*Exemples:*
```
CREATE ROLE nom_usuari LOGIN PASWORD 'la_password';

CREATE USER nom_usuari PASSWORD 'la_password';
```

Si creem un rol que no pot fer login el podem entendre com un grup. Podem afegir un rol a un altre rol i així els membres tenen els mateixos permisos que el rol pare.

## Accés
```
$ psql -h ip_servidor|nom_servidor -U nom_usuari nom_bd
```

