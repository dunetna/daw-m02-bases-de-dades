-- --------------------------------------------
--    Funcions d'usuari
-- --------------------------------------------

-- 1.- Llistar l'identificador i el nom dels representants de vendes. Mostrar un camp anomenat "result" que mostri 0 si la quota és inferior a les vendes, en cas contrari ha de mostrar 1 a no ser que sigui director d'oficina, en aquest cas ha de mostrar 2.


-- 2.- Llistar tots els productes amb totes les seves dades afegint un nou camp anomenat "div". El camp div ha de contenir el resultat de la divisió entre el preu i les existències. En cas de divisió per zero, es canviarà el resultat a 0.


-- 3.- Afegir una condició a la sentència de l'exercici anterior per tal de nomès mostrar aquells productes que el valor del camp div és menor a 1.


-- 4.- Donat l'identificador d'un client que retorni la importància del client, és a dir, el percentatge dels imports de les comandes del client respecte al total dels imports de les comandes.


-- 5.- Calcular el que s'ha deixat de cobrar per a un producte determinat.
-- És a dir, la diferència que hi ha entre el que val i el que li hem cobrat
-- al total de clients.


-- 6.- Crea una funció que si li passem dos columnes com les de "ventas" i "cuota". Ha de retornar una columna amb el valor de restar la quota a les ventes.


-- 7.- Fes una funció que donat un identificador de representant de vendes retorni l'identificador dels clients que té assignats amb el seu límit de crèdit.


-- 8.- Crear una funció promig_anual(venedor, anyp) que retorni el promig d'imports de comandes del venedor durant aquell any.


-- 9.- Crear una funció max_promig_anual(anyp) que retorni el màxim dels promitjos anuals de tots els venedors. Useu la funció de l'exercici anterior.


-- 10.- Creeu una funció promig_anual_tots(anyp) que retorni el promig anual de cada venedor durant l'any indicat. Useu funcions creades en els exercicis anteriors.


-- 11.- Feu una funció que retorni tots els codis dels clients que no hagin comprat res durant el mes introduït.


-- 12.- Funció anomenada MaximMes a la que se li passa un any i retorna el mes en el que hi ha hagut les màximes vendes (imports totals del mes).


-- 13.- a) Crear una funció baixa_rep que doni de baixa el venedor que se li passa per paràmetre i reassigni tots els seus clients al venedor que tingui menys clients assignats (si hi ha empat, a qualsevol d'ells).
--      b) Com usarieu la funció per donar de baixa els 3 venedors que fa més temps que no han fet cap venda?


-- 14.- Crear una funció anomenada "nclientes" que donat un identificador d'un representant de vendes ens retorni el nombre de clients que te assignats. Si l'entrada és nul·la s'ha de retornar un valor nul. 


-- 15.- Crear una funció anomenada "natendidos" que donat un identificador d'un representant de vendes ens retorni el nombre de clients diferents que ha atès. Si l'entrada és nul·la s'ha de retornar un valor nul.


-- 16.- Crear una funció anomenada "totalimportes" que donat un identificador d'un representant de vendes ens retorni la suma dels imports de les seves comandes. Si l'entrada és nul·la s'ha de retornar un valor nul.


-- 17.- Crear una funció anomenada "informe_rep" que ens retorni una taula amb l'identificador del representant de vendes,
-- el resultat de "nclientes", "natendidos" i "totalimportes". Si a la funció se li passa un identificador de representant
-- de vendes només ha de retornar la informació relativa a aquest representant de vendes. En cas de passar un valor nul a
-- la funció aquesta ha de donar la informació de tots els representants de vendes.   


-- 18.- Crear una funció i les funcions auxiliars convenients per obtenir la següent informació:
-- Donat l'identificador d'un producte obtenir una taula amb les següents dades:
--  * Identificador de producte i fabricant.
--  * Nombre de representants de vendes que han venut aquest producte.
--  * Nombre de clients que han comprat el producte.
--  * Mitjana de l'import de les comandes d'aquest producte.
--  * Quantitat mínima i quantitat màxima que s'ha demanat del producte en una sola comanda.


-- 19.- Crear una funció que donada una oficina ens retorni una taula identificant els productes i mostrant la quantitat d'aquest producte que s'ha venut a l'oficina.


-- 20.- Crear les funcions necessàries per aconseguir el següent resultat:
-- Cridant resumen_cliente() ha de retornar una taula amb la amb els identificadors dels clients, la suma de les seves compres, nombre de comandes realitzades i nombre de representants de vendes que l'han atès.
-- Cridant resumen_cliente(num_clie) ha de retornar una taula amb els identificador dels representants de vendes i el nombre de comandes que ha realitzat el client amb aquest representant de vendes.
-- Cridant resumen_cliente(num_clie, num_empl) ha de retornar una taula amb el nombre de productes diferents que ha demanat el client especificat al representant de vendes especificat i la mitja de l'import de les comandes que ha realitzat el client especificat al representant de vendes especificat. 


-- 21.- Crear una funció el_millor() que retorni totes les dades del venedor que ha venut més (major total d'imports) durant l'any en curs.


-- 22.- Calcular el descompte fet a un client concret, respecte totes les comandes del client.
-- Cal crear dos funcions auxiliars:
-- La primera obtindrà el total dels imports de les comandes d'un client determinat.
-- La segona serà una funció preu de "comanda abstracta" que necessitarà el producte en qüestió i la quantitat de productes demanats.


