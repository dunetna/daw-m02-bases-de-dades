#!/bin/bash
# Copyright 2016 Mònica Ramírez Arceda <mramirez@escoladeltreball.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

echo -n "Root "
su -c "yum install postgresql-server -y; 
       postgresql-setup initdb; 
       systemctl start postgresql; 
       systemctl enable postgresql; 
       echo 'postgres' | passwd postgres --stdin"
echo -n "Postgres "
su postgres -c "psql template1 -c \"CREATE USER `whoami` CREATEDB;\""
psql template1 -c "CREATE DATABASE training;"
psql training -f /tmp/training.sql
