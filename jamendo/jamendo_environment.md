# Entorn Jamendo

## Sistema gestor de base de dades

Per poder accedir a la base de dades des del programa de consulta de dades s'ha d'editar el fitxer /var/lib/pgsql/data/pg_hba.conf

A la part final del fitxer canviar la línia següent:
```
host    all         all         127.0.0.1/32          ident
```
per:
```
host    all         all         127.0.0.1/32          trust
```

Reiniciar el servei de gestor de bases de dades:
```
systemctl restart postgresql
```

## Programa per obtenir la base de dades: get-jamendo-db

Descarregar el programa.
Permetre l'execució del programa:
```
chmod +x ./get-jamendo-db
```

Executar el programa:
```
./get-jamendo-db 5 4
                 ^ ^
                 |  `- Factor
                  `- Numero d'artistes a descarregar
```

Exemple de fitxer obtingut: jamendo_5_4_201603151337.sql

Importar la base de dades:
```
cat jamendo_5_4_201111301249.sql | psql template1
```

Accedir a la base de dades:
```
psql jamendo
```

## Programa de consulta de dades: DBClient

Per usar el programa necessitem el controlador per connectar amb el sistema gestor de bases de dades postgresql.

Descarregar el controlador per connectar a la base de dades: [https://jdbc.postgresql.org/download.html](https://jdbc.postgresql.org/download.html)

Per usar el controlador amb DrJava:
* Menú "Edit" -> "Preferences..."
* A la secció "Resource Locations": afegir un "Classpath" seleccionant el fitxer descarregat.

