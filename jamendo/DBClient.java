/*
 * ProgramTUI.java        1.0 08/11/2011
 *
 * Models a collection of queries to Jamendo database.
 *
 * Copyright 2011 Joaquim Laplana Tarragona <jlaplana@escoladeltreball.org>
 *                Mònica Ramírez Arceda <mramirez@escoladeltreball.org>
 *                Simó Albert i Beltran <salbert@escoladeltreball.org> *                
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.util.Scanner;

public final class DBClient extends JFrame {

    // Attributes
    private Connection connection = null;    
    private Statement statement = null;
    private String stringQuery;

    /**
     * Constructor.
     * 
     * @param select a query to execute.
     */
    public DBClient(final String select) {
        super();
        this.stringQuery = select;
        int rowCount = 0;
        try {
            // Get a connection to database postgresql.
            Class.forName("org.postgresql.Driver").newInstance();
            final String url = "jdbc:postgresql://localhost/jamendo";
            // final String username = "user";
            // final String password = "pass";
            // connection = DriverManager.getConnection(url, username,
            // password);
            connection = DriverManager.getConnection(url);
            // Create a statement, execute your stringQuery
            statement = connection.createStatement();
            final ResultSet rs = statement.executeQuery(this.stringQuery);
            final ResultSetMetaData md = rs.getMetaData();
            // Count how many columns are in the result set
            int colCount = md.getColumnCount();
            // Find table names for column headings
            final String[] headings = new String[colCount];
            for (int i = 0; i < colCount; i++) {
                headings[i] = md.getColumnName(i + 1);
            }
            // Convert the result set into an array of Objects
            final Object[][] rows = this.getObjects(rs);
            // Count the number of records or resgisters obtained
            rowCount = rows.length;
            // Initialize a JTable with the rows of objects and column headings
            final JTable table = new JTable(rows, headings);
            // Add the table to a JScrollPane to make it display nicely
            final JScrollPane scrollPane = new JScrollPane(table);

            add(scrollPane);
        } catch (final SQLException sqle) {
            // sqle.printStackTrace();
            // JOptionPane.showMessageDialog(this,
            // "S'ha produït un error SQL: "+ sqle.getMessage(), "Excepció",
            // JOptionPane.INFORMATION_MESSAGE);
            JOptionPane.showMessageDialog(this, "S'ha produït un error SQL", "Excepció", JOptionPane.INFORMATION_MESSAGE);
        } catch (final Exception e) {
            JOptionPane.showMessageDialog(this, "S'ha produït un error", "Excepció", JOptionPane.INFORMATION_MESSAGE);
        }
        try {
            statement.close();
            connection.close();
        } catch (final SQLException ex) {
            // ex.printStackTrace();
            JOptionPane.showMessageDialog(this, "S'ha produït un error SQL", "Excepció", JOptionPane.INFORMATION_MESSAGE);
        }
        setSize(700, 500);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        setTitle("La consulta retorna " + rowCount + " registres");
        setVisible(true);
    }

    /**
     * Determines the object matrix containing the register values ready to be
     * displayed in a JTable.
     * 
     * @param rs the ResultSet obtained from query
     * @result the object matrix
     */
    public Object[][] getObjects(final ResultSet rs) {
        try {
            // Find how many columns are in the result set
            final ResultSetMetaData metaData = rs.getMetaData();
            final int colCount = metaData.getColumnCount();
            final ArrayList<Object> rows = new ArrayList<Object>();
            Object[] row = null;
            while (rs.next()) {
                // For each row in the result set, put every column into a
                // temporary Object array
                row = new Object[colCount];
                for (int a = 0; a < colCount; a++) {
                    row[a] = rs.getObject(a + 1);
                }
                // Add the temporary Object array to the array list
                rows.add(row);
            }
            // Convert the array list to object matrix
            final Object[][] data = (Object[][]) rows.toArray(new Object[0][0]);
            return data;
        } catch (final SQLException ex) {
            JOptionPane.showMessageDialog(this, "S'ha produït un error SQL", "Excepció", JOptionPane.INFORMATION_MESSAGE);
            System.exit(0);
            return null;
        }
    }

    /**
     * Print the main menu.
     */
    public static void printMenu() {
        System.out.print("\n\nJamendo\n\n");
        System.out.println("1. Tots els artistes");
        System.out.println("         Aquesta consulta mostra tots els artistes amb tota la seva informació.");
        System.out.println("2. Cançons en frances");
        System.out.println("         Aquesta consulta bla bla blabla bla blabla bla blabla bla bla");
        System.out.println("3. Error de sintaxi");
        System.out.println("         Aquesta consulta conté un error de sintaxi");
        System.out.println("0. Sortir");
    }

    /**
     * Modelizes a TUI.
     * 
     * @param args Not used.
     */
    public static void main(final String[] args) {
        Scanner s = new Scanner(System.in);
        String select = "";
        // Ask for an option
        DBClient.printMenu();
        System.out.print("Quina opció vols ? ");
        int n = s.nextInt();        
        // Depending on the option, build a different query
        while (n != 0) {
            switch (n) {
            case 1:
                select = "SELECT * FROM artist";
                break;
            case 2:
                select = "SELECT * "
                       + "FROM track "
                       + "WHERE lang = 'fr' "
                       + "AND duration > 300 ORDER BY duration DESC";
                break;
            case 3:
                select = "SELECT * "
                       + "FROM track"
                       + "WHERE duration > 300 ORDER BY duration DESC";
                break;
            default:
                System.out.println("ERROR: Opció no vàlida");
            }
            // Execute the query
            new DBClient(select);
            // Ask for an option
            DBClient.printMenu();
            System.out.print("Quina opció vols? ");
            n = s.nextInt();
        }
    }
}
